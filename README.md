# DSAO-02 (Foundations of data structures)
Welcome to DSAO-02 batch at Sunbeam Infotech.

## Syllabus
* Sorting
* Searching
* Stack
* Queue
* Linked List
* Tree Terminologies
* Binary Search Tree
* Heaps

For more details and registration: http://sunbeaminfo.com/modular-courses.php?mdid=33

## Details
* Data structures and algorithms codes will be implemented in Java programming language.
* Daily classwork codes and slides/notes will be shared in this repository.
* Corresponding C++ and Python codes will be shared at the end of session in the same repository.
